FROM python:3.7

MAINTAINER Vadim Shmakov <v@shmakov.me>

RUN apt-get update -y && apt-get install -y

COPY ./start.sh /start.sh
RUN chmod +x /start.sh

COPY  . /app/
WORKDIR /app

RUN pip install --no-cache-dir -r /app/requirements.txt

CMD ["/start.sh"]
