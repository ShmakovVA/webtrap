# Webtrap

`Log everything you want`

## VENV
steps to prepare: 
1. clone it 
2. go to project folder 
3. create venv and switch to it
3. run `pip install -r requirements.txt`
4. run `PORT=8001 gunicorn main:app`

## DOCKER
steps to prepare:
1. install docker
2. install docker-compose
3. clone it
4. go to project folder
5. create .env file in the project folder (like in example below)
6. run: `docker-compose up -d --build` 

_note_: in dev mode (FLASK_DEBUG=true in .env) we not start app - only build

##.env example
should be in the root folder

```
PORT=8001
FLASK_DEBUG=false
```