from flask import jsonify, make_response


class ResponseMaker():
    def __init__(self, msg=None):
        self.msg = msg or 'default message'

    def _make_response(self, msg=None, is_fail=False, data=None):
        msg = msg or self.msg
        data = data or {}
        return make_response(
            jsonify(
                {'msg': msg,
                 'status': 'success' if not is_fail else 'failed',
                 'data': data}
            ), 200)

    def success_response(self, msg=None, data=None):
        return self._make_response(msg='You are awesome !', data=data)

    def failed_response(self, msg=None, data=None):
        return self._make_response(msg='You are awesome BUT ...', data=data,
                                   is_fail=True)


response_maker = ResponseMaker()
