from settings import HTTP_ALLOWED_METHODS, FAIL_PARAMS, ALLOWED_URL_PATHS
import logging
from logging.handlers import RotatingFileHandler
import sys


def setup_custom_logger(name, is_rotating=True):
    formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s',
                                  datefmt='%Y-%m-%d %H:%M:%S')
    if is_rotating:
        handler = RotatingFileHandler('log.txt', maxBytes=2048)
    else:
        handler = logging.FileHandler('log.txt')
    handler.setFormatter(formatter)
    screen_handler = logging.StreamHandler(stream=sys.stdout)
    screen_handler.setFormatter(formatter)
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(handler)
    logger.addHandler(screen_handler)
    return logger


class Verifier():
    def __init__(self, request, logger):
        self.url = request.url.split('?')[0]
        self.full_path = request.full_path.split('?')[0]
        self.args = request.args.to_dict(flat=False)
        self.method = request.method
        self._logger = logger
        self.status = {}

    @property
    def request_as_str(self):
        return f'{self.method} : {self.url} : {self.args}'

    def _build_log_error_str(self, str):
        return f'{self.request_as_str} --> {str}'

    def _log_request_as_it(self):
        self._logger.info(self.request_as_str)

    def _check_method(self):
        if self.method not in HTTP_ALLOWED_METHODS:
            self._logger.error(self._build_log_error_str(
                f'method={self.method} is looks like method error')
            )
            return True
        return False

    def _check_params(self):
        is_error = False
        for fail_param, fail_value in FAIL_PARAMS.items():
            fail_request_arg = self.args.get(fail_param, None)
            if fail_request_arg is not None and fail_value in fail_request_arg:
                self._logger.error(self._build_log_error_str(
                    f'{fail_param}={fail_value} is looks like param error')
                )
                is_error = True
        return is_error

    def _check_url(self):
        if self.full_path not in ALLOWED_URL_PATHS:
            self._logger.error(self._build_log_error_str(
                f'url={self.full_path} is looks like url path error')
            )
            return True
        return False

    def verify_and_log_request(self):
        self._log_request_as_it()
        self.status = {'url_error': self._check_url(),
                       'method_error': self._check_method(),
                       'params_error': self._check_params()}
        return self.status
