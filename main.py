from flask import Flask, request, session
from log_request import Verifier, setup_custom_logger
from settings import PORT, HTTP_METHODS, SECRET_KEY
from helpers import response_maker

app = Flask(__name__)
app.config['SECRET_KEY'] = SECRET_KEY

logger = setup_custom_logger(__name__)


def __verified_status():
    return session.get('verify_query_status', None)


def query(request):
    return Verifier(request=request, logger=logger).verify_and_log_request()


@app.before_request
def before_request():
    session['verify_query_status'] = query(request=request)


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>', methods=HTTP_METHODS)
def catch_all(path):
    status = __verified_status()
    if status is not None:
        data = {'url_error': status.get('url_error', False),
                'params_error': status.get('params_error', False)}
        if any(data.values()):
            return response_maker.failed_response(data=data)
    else:
        return response_maker.failed_response(data={'session_error': True})
    return response_maker.success_response()


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=PORT)
