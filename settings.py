import os

PORT = int(os.environ.get('PORT', 8000))

HTTP_METHODS = ['GET', 'HEAD', 'POST', 'PUT', 'DELETE', 'CONNECT',
                'OPTIONS', 'TRACE', 'PATCH']
HTTP_ALLOWED_METHODS = ['GET']

FAIL_PARAMS = {'invalid': '1', 'notawaiting': '1'}

ALLOWED_URL_PATHS = ['/api']

SECRET_KEY = os.environ.get('SECRET_KEY', 'hssekgsl;k234fdqr3')
