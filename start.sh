#! /usr/bin/env sh
set -e

# Start Gunicorn
if [ "$FLASK_DEBUG" = "false" ]; then
    exec gunicorn --workers=4 main:app
fi
