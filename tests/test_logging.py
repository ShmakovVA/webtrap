from unittest import TestCase
from main import app

app_client = app.test_client()


class RequestProcessingTests(TestCase):

    def test_catching_url_error(self):
        response = app_client.get('/wrong-path-to-request',
                                  query_string=dict(arg1='a', arg2='b'))
        expected_status = {'data': {'params_error': False, 'url_error': True},
                           'status': 'failed',
                           'msg': 'You are awesome BUT ...'}
        self.assertDictEqual(response.json, expected_status)

    def test_catching_invalid_param_error(self):
        response = app_client.get('/api',
                                  query_string=dict(invalid='1', arg2='b'))
        expected_status = {'data': {'params_error': True, 'url_error': False},
                           'status': 'failed',
                           'msg': 'You are awesome BUT ...'}
        self.assertDictEqual(response.json, expected_status)

    def test_catching_notawaiting_param_error(self):
        response = app_client.get('/api',
                                  query_string=dict(invalid='0',
                                                    notawaiting='1'))
        expected_status = {'data': {'params_error': True, 'url_error': False},
                           'status': 'failed',
                           'msg': 'You are awesome BUT ...'}
        self.assertDictEqual(response.json, expected_status)

    def test_logging_and_catching_url_plus_param_error(self):
        with self.assertLogs('main', level='INFO') as cm:
            response = app_client.get('/wrong-path-to-request',
                                      query_string=dict(invalid='1', arg2='b'))
            expected_status = {
                'data': {'params_error': True, 'url_error': True},
                'status': 'failed',
                'msg': 'You are awesome BUT ...'}
            self.assertDictEqual(response.json, expected_status)
            self.assertEqual(cm.output, [
                "INFO:main:GET : http://localhost/wrong-path-to-request : "
                "{'invalid': ['1'], 'arg2': ['b']}",
                "ERROR:main:GET : http://localhost/wrong-path-to-request : "
                "{'invalid': ['1'], 'arg2': ['b']} "
                "--> url=/wrong-path-to-request is looks like url path error",
                "ERROR:main:GET : http://localhost/wrong-path-to-request : "
                "{'invalid': ['1'], 'arg2': ['b']} "
                "--> invalid=1 is looks like param error"
            ])

    def test_logging_not_allowed_method_as_error(self):
        with self.assertLogs('main', level='INFO') as cm:
            response = app_client.put('/api',
                                      query_string=dict(arg1='a', arg2='b'))
            expected_status = {'data': {},
                               'status': 'success',
                               'msg': 'You are awesome !'}
            self.assertDictEqual(response.json, expected_status)
            self.assertEqual(cm.output, [
                "INFO:main:PUT : http://localhost/api : "
                "{'arg1': ['a'], 'arg2': ['b']}",
                "ERROR:main:PUT : http://localhost/api : "
                "{'arg1': ['a'], 'arg2': ['b']} --> "
                "method=PUT is looks like method error"
            ])


if __name__ == '__main__':
    unittest.main()
