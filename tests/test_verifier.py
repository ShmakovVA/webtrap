from unittest import TestCase
from log_request import Verifier
from main import app
from werkzeug.datastructures import ImmutableMultiDict


class fake_request():
    def __init__(self, url, method, full_path, args):
        self.url = url
        self.method = method
        self.args = ImmutableMultiDict(args)
        self.full_path = full_path


class RequestVerifierTests(TestCase):

    def _request(self, url, full_path, method, args):
        return Verifier(request=fake_request(url=url, full_path=full_path,
                                             args=args, method=method),
                        logger=app.logger).verify_and_log_request()

    def test_params_error(self):
        expected_status = {'method_error': False,
                           'params_error': True,
                           'url_error': False}
        self.assertDictEqual(self._request(url='http://localhost:8001/api',
                                           full_path='/api',
                                           args={'invalid': '1'},
                                           method='GET'),
                             expected_status)

    def test_url_error(self):
        expected_status = {'method_error': False,
                           'params_error': False,
                           'url_error': True}
        self.assertDictEqual(self._request(url='http://localhost:8001/api/sdf/',
                                           full_path='/api/sdf/',
                                           args={'invalid': '0'},
                                           method='GET'),
                             expected_status)

    def test_not_allowed_method_case(self):
        expected_status = {'method_error': True,
                           'params_error': False,
                           'url_error': False}
        self.assertDictEqual(self._request(url='http://localhost:8001/api',
                                           full_path='/api',
                                           args={'invalid': '0'},
                                           method='PUT'),
                             expected_status)

    def test_all_errors(self):
        expected_status = {'method_error': True,
                           'params_error': True,
                           'url_error': True}
        self.assertDictEqual(self._request(url='http://localhost:8001/sdf',
                                           full_path='/sdf',
                                           args={'invalid': '1'},
                                           method='POST'),
                             expected_status)


if __name__ == '__main__':
    unittest.main()
